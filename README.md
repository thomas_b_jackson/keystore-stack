# Keystore Stack

Install and update an SSL cert via a keystore suitable for use with an Java web app.

* Provides a task only - no infrastructure
* Intended for use as a sub-service on Java-based services that terminate SSL (e.g. brikit/jira and brikit/confluence)

# Dependencies

yac!

pip install yac