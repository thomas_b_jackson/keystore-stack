import re
from urlparse import urlparse
from yac.lib.inputs import get_inputs, get_stack_service_inputs_value
from yac.lib.variables import get_variable, set_variable
from yac.lib.stack import get_stack_name, stack_exists
from yac.lib.template import apply_stemplate

# populate dynamic params for the jira service
def get_value(params):

    print "gathering inputs via <servicefile>/lib/inputs.py ..."

    # get inputs per service-inputs in the Servicefile
    get_inputs(params)