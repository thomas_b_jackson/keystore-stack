#!/usr/bin/env python

import time, os, subprocess, json, sys, boto3, botocore, urlparse
from colorama import Fore, Style

from yac.lib.stack import get_stack_name, get_stack_state, cp_file_to_s3
from yac.lib.stack import stop_service_blocking, start_service, get_ec2_sgs
from yac.lib.stack import get_running_ec2s, get_ecs_service, cp_file_to_s3
from yac.lib.variables import get_variable, set_variable
from yac.lib.state import save_state_s3
from yac.lib.naming import get_resource_name
from yac.lib.task import handle_task_inputs
from yac.lib.file import dump_file_contents, get_dump_path
from yac.lib.container.api import find_container_by_image, get_connection_str
from yac.lib.container.start import execute


def task_handler(params, stack_template, inputs, conditional_inputs):

    print "task keystore starting..."

    # get inputs associated with this task
    task_params = handle_task_inputs(inputs, conditional_inputs)
    
    # first build the keystore
    local_keystore_path, err = build_keystore(params, task_params)

    if not err and local_keystore_path:
        
        # next install the keystore
        install_keystore(local_keystore_path, params, task_params)

    elif err:

        print "keystore install failed with error: %s"%err

    print "task keystore complete"

def build_keystore(params, task_params):

    # for making a new truststore for use with Jira
    # input is the brikit wildcard cert issued by godaddy
    # cert is in the file wildcard.brikit.com.pem
    # >> cat brikit-wildcard-cert-private-key.pem wildcard.brikit.com.pem godaddy/gdig2.pem godaddy/gdroot-g2.pem > wildcard.brikit.com_fc_keyed.pem
    # >> openssl pkcs12 -export -in wildcard.brikit.com_fc_keyed.pem -out brikit_wildcard_keystore.pkcs12 -name jira -noiter -nomaciter -password pass:jira

    # load the private key into a string
    private_key_str = load_file(get_variable(params,"private-key-path"))

    # load the cert
    ssl_cert_str = load_file(get_variable(task_params,"ssl-cert-path"))

    # load the CA intermediate cert
    ca_intermediate_cert_str = load_file(get_variable(params,"ca-intermediate-cert-path"))

    # load the CA root cert
    ca_root_cert_str = load_file(get_variable(params,"ca-root-cert-path"))

    # combine the key with the certs and write to a file that will serve as input to the openssl utility
    # order here is important!
    keyed_certs = private_key_str + ssl_cert_str + ca_intermediate_cert_str + ca_root_cert_str

    service_alias = get_variable(params,"service-alias")
    local_keyed_cert_path = dump_file_contents(keyed_certs, service_alias, "keyed_cert.pem")

    # form the path that the keystore should get written to
    local_keystore_path = os.path.join( get_dump_path(service_alias), "keystore.pkcs12" )

    # get the key alias and pwd
    key_alias = get_variable(params, 'keystore-key-alias')
    key_pwd = get_variable(params, 'keystore-key-password')

    # use openssl to create the keystore
    keystore_create_cmd = "openssl pkcs12 -export -in %s -out %s -name %s -noiter -nomaciter -password pass:%s"%(local_keyed_cert_path,local_keystore_path,key_alias,key_pwd)

    print "openssl cmd: %s"%keystore_create_cmd
    # run the command
    err = ""
    try:
        subprocess.check_output( keystore_create_cmd, stderr=subprocess.STDOUT, shell=True )

    except subprocess.CalledProcessError as error:
        err = error.output

    return local_keystore_path, err

def install_keystore(cert_path, params, task_params):

    host_ip = get_host_ip(params)

    if (host_ip and os.path.exists(cert_path)):

        # get the name of the ECS service that serves the cert
        ecs_service_name = get_variable(params,"ecs-service-name")

        # get the name of the stack that corresponds to this app and env
        stack_name = get_variable(params,"stack-name")

        # stop the ECS service that serves the cert via the keystore, and block indefinitely until we have 
        # confirmed it has stoppped
        stop_service_blocking(stack_name, ecs_service_name)

        # install the cert on the host
        install_keystore_on_host(cert_path, params, host_ip)

        # restart the ECS service that serves the cert
        start_service(stack_name, ecs_service_name)
    
    else:

        print "\nA single, running, EC2 instance could not be found for stack: %s. Installation aborted."%stack_name  

def get_host_ip(params):

    host_ip = ""

    # get the name of the stack that corresponds to this app and env
    stack_name = get_stack_name(params)

    ec2_instances = get_running_ec2s(stack_name)

    if ec2_instances and len(ec2_instances):

        ec2_instance = ec2_instances[0]

        host_ip = ec2_instance['PublicIpAddress']

    return host_ip

def install_keystore_on_host(keystore_path, params, target_host_ip):

    # upload the cert to S3 ...
    s3_url = get_variable(params, "keystore-s3-path")

    cp_file_to_s3(keystore_path, s3_url)

    # ... then pull it onto the host using the backups container

    # get connection string for the docker remote api on the target host
    docker_api_conn_str = get_connection_str( target_host_ip )

    # get the full name of the backups container running on the host
    container_name = get_backups_container_name(target_host_ip, docker_api_conn_str)

    if container_name:

        host_path = get_variable(params, "keystore-host-path")

        # form the download comand, where the script is per the api of the backups container
        container_cmd = "python -m src.restore_file %s %s"%(s3_url, host_path)
         
        user_msg = "Loading the keystore to the host via the %s container on %s using command:\n%s"%(container_name,
                                                                                                     target_host_ip,
                                                                                                     container_cmd)

        raw_input(user_msg + "\nHit <enter> to continue..." )

        # execute the command on the pg-pitr container
        if True:

            execute(
                container_name=container_name,
                exec_cmd=container_cmd,
                connection_str=docker_api_conn_str)

    else:
        user_msg = ("Installation aborted. This task leverages the nordstromsets/backups container." +
                    " Could not find a running instance of the backups container on %s"%(target_host_ip))

        raw_input(user_msg + "\nHit <enter> to continue..." )


def get_backups_container_name(target_host_ip, docker_api_conn_str):

    container_name = ""

    container_desc = find_container_by_image( "backups", docker_api_conn_str)

    if "Names" in container_desc and len(container_desc["Names"])==1:

        # docker py sometimes as a '/' char in the prefix of the name
        container_name = container_desc["Names"][0].strip("/")

    return container_name

def load_file(file_path):

    contents_str = ""

    if "s3://" in file_path:
        # path is an s3 url
        # print "loading %s from s3"%file_path
        contents_str = load_file_s3(file_path)

    elif os.path.exists(file_path):
        # path is local
        # print "loading %s from local disk"%file_path
        with open(file_path) as file_arg_fp:
            contents_str = file_arg_fp.read()

    else:
        print "the %s file does not exist or can't be found"%file_path

    return contents_str

def load_file_s3(s3_url):

    contents_str = ""

    client = boto3.client('s3')

    # parse the URL
    url_parts = urlparse.urlparse(s3_url)

    # bucket is the netloc 
    bucket = url_parts.netloc

    # make sure bucket was parsed correctly 
    if bucket and bucket_exist(bucket):
        
        # key is the path with the leading / stripped
        file_key = url_parts.path.strip('/')

        if key_exists(bucket,file_key):

            file_obj = client.get_object(Bucket=bucket,Key=file_key)

            contents_str = file_obj['Body'].read()

        else:
            print 'Bucket exists but file specied does not exist'
    else:
        print 'Bucket input does not exist'

    return contents_str

def bucket_exist(bucket):
    
    exists = False
    client = boto3.client('s3')

    try:
        client.head_bucket(Bucket=bucket)
        exists = True
    except botocore.exceptions.ClientError as e:
        exists = False

    return exists

def key_exists(bucket,file_key):
    
    exists = False

    s3 = boto3.resource('s3')
    try:
        s3.Object(bucket, file_key).load()
        exists = True
    except botocore.exceptions.ClientError as e:
        exists = False

    return exists    