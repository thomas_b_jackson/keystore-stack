import unittest, os, random
from lib.keystore import build_keystore, install_keystore_on_host

class TestCase(unittest.TestCase):

    def test_build_keystore(self): 
        
        params = {

          "ca-intermediate-cert-path": {
            "comment": "The path (s3 url or local) to the intermediate certificate of the certificate authority",
            "value": "s3://brikit-certs/godaddy/gd-intermediate-g2.pem"
          },
          "private-key-path": {
            "comment": "The path (s3 url or local) to the private key used to generate the certicate request",
            "value": "s3://brikit-secrets/cert/brikit-wildcard-cert-private-key.pem"
          },   
          "ca-root-cert-path": {
            "comment": "The path (s3 url or local) to the root certificate of the certificate authority",
            "value": "s3://brikit-certs/godaddy/gd-root-g2.pem"
          },  
          "keystore-s3-path": {
            "comment": "The the S3 path where the created keystore should be staged (typically the same S3 location where the keystore is backed up)",
            "value": "s3://brikit-confluence/confluence/dev/confluence_keystore.pkcs12"
          },
          "keystore-key-alias": {
            "comment": "The key aliase",
            "value": "confluence"
          },  
          "keystore-key-password": {
            "comment": "The key pwd",
            "value": "confluence"
          }
        }

        task_params = {
          "ssl-cert-path": {
            "comment": "The path to the ssl cert",
            "value": "/Users/x0ox/Dropbox/ridgeline_solutions/brikit/certs/wildcard.brikit.com.pem"
          }
        }

        local_keystore_path, err = build_keystore(params, task_params)

        print "path: %s, err: %s"%(local_keystore_path, err)

        self.assertTrue(not err and os.path.exists(local_keystore_path)) 
          
    def test_install_keystore_on_host(self):

      params = {

          "s3-bucket": {
            "comment": "The bucket where the keystore can be staged",
            "value": "brikit-confluence"
          },
          "keystore-host-path": {
            "comment": "The path on the host where the created keystore should be installed",
            "value": "/var/local/atlassian/confluence/confluence_keystore.pkcs12"
          }          
        }

      target_host_ip = "52.89.171.246"
      keystore_path = "/Users/x0ox/.yac/tmp/keystore.pkcs12"

      install_keystore_on_host(keystore_path, params, target_host_ip)